import React from 'react';

function WeFailed() {
    return (
        <div className="we-failed text-center" >
            <h2>We failed to load the data</h2>
            <p>We are working to get it back up!</p>
        </div>
    );
}

export default WeFailed;