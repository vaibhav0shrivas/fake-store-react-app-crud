import React from "react";

function FooterFakeStore() {
    return (
        <div className="container-fluid bg-dark text-light p-0 w-100 mt-0">
            <footer className="section-footer border-top">
                <div className="container-fluid">
                    <section className="footer-top p-0">
                        <div className="text-center">

                            <h6 className=" m-2">Everything you need!</h6>

                        </div>
                    </section>
                    <section className="footer-copyright">
                        <p className="float-left text-muted">
                            &copy; 2023 FakeStore All rights reserved
                        </p>
                        <p className="float-right text-muted">
                            <a href="#!">Privacy &amp; Cookies</a>
                            &nbsp; &nbsp; <a href="#!">Accessibility</a>
                        </p>
                    </section>
                </div>
            </footer>
        </div>
    );
}

export default FooterFakeStore;

