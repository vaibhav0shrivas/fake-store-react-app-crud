import React from 'react';
import { Link } from "react-router-dom";
import ButtonConfirm from './ButtonConfirm';

class Inventory extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectAllIsChecked: false,
            showConfirmationOnDeleteAll: false,
            checkboxIsCheckedFor: {},

        };

    }

    componentDidMount() {
        document.title = "Fake Store | Inventory";
    };

    getIdsOfProductToBeDeleted = () => {

        let idsForDelete = Object.entries(this.state.checkboxIsCheckedFor)
            .filter((checkboxIsCheckedForObject) => {
                return checkboxIsCheckedForObject[1] === true;

            })
            .map((checkboxIsCheckedForObject) => {
                return Number(checkboxIsCheckedForObject[0].slice(9));
            });

        return idsForDelete;


    };

    initiateDelete = (productIdArray) => {
        if (productIdArray === undefined) {
            let deleteIds = this.getIdsOfProductToBeDeleted();

            if (Array.isArray(deleteIds) && deleteIds.length > 1) {
                this.props.deleteProductHandler(deleteIds);
            }

        } else {
            this.props.deleteProductHandler(productIdArray);

        }

    };

    toggleAllCheckbox = (isChecked) => {
        let checkboxIDs = this.props.productsData.reduce((accumulator, productObject) => {
            accumulator["checkbox_" + productObject.id] = isChecked;
            document.getElementById("checkbox_" + productObject.id).checked = isChecked;
            return accumulator
        }, {});

        this.setState({
            checkboxIsCheckedFor: { ...checkboxIDs },
        })

    };

    handleCheckbox = (event) => {

        if (event.target.id === "selectAll") {

            this.setState({
                selectAllIsChecked: event.target.checked,
            },
                () => {
                    this.toggleAllCheckbox(event.target.checked);
                }
            )

        } else {
            if (event.target.id in this.state.checkboxIsCheckedFor) {
                this.setState((prevState) => {
                    return {
                        checkboxIsCheckedFor: {
                            ...prevState.checkboxIsCheckedFor,
                            [event.target.id]: event.target.checked
                        }
                    }
                });
            }

        }

    };

    toggleDeleteConfirmation = () => {
        this.setState({
            showConfirmationOnDeleteAll: true,
        })
    };

    confirmationForDelete = (isConfirmed) => {

        this.setState({
            showConfirmationOnDeleteAll: false,
        },
            () => {
                if (isConfirmed === true) {
                    this.initiateDelete();

                }
            }
        );
    };

    componentDidMount() {

        let checkboxIDs = this.props.productsData.reduce((accumulator, productObject) => {
            accumulator["checkbox_" + productObject.id] = false;
            return accumulator
        }, {});

        this.setState({
            checkboxIsCheckedFor: { ...checkboxIDs },
        })

    }

    render() {
        return (
            <main className="flex-grow-1  min-vh-100">
                <div className="container">
                    <div className="table-wrapper">
                        <div className="table-title">
                            <div className="row">
                                <div className="col-sm-5">
                                    <h2>Manage Inventory</h2>
                                </div>
                                <Link className="col-sm-3 btn btn-success p-auto m-auto"
                                    to={`/addProduct`}>
                                    <i className="fa-regular fa-square-plus"></i>
                                    &nbsp;&nbsp;
                                    <span>Add New Product</span>
                                </Link>
                                <div className="col-sm-3 btn btn-danger p-auto m-auto"
                                    onClick={(event) => {
                                        this.toggleDeleteConfirmation();
                                    }} >
                                    <i className="fa-regular fa-square-minus"></i>
                                    &nbsp;&nbsp;
                                    <span>Delete Selected</span>
                                </div>
                                {
                                    this.state.showConfirmationOnDeleteAll &&
                                    <div className="col-12 text-center">
                                        <ButtonConfirm
                                            buttonMessage="Multiple Delete?"
                                            buttonHandler={this.confirmationForDelete}
                                        />
                                    </div>
                                }



                            </div>
                        </div>
                        <table className="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        <span className="custom-checkbox">
                                            <input type="checkbox"
                                                id="selectAll"
                                                onChange={(event) => {
                                                    this.handleCheckbox(event);
                                                }} />
                                            <label htmlFor="selectAll"></label>
                                        </span>
                                    </th>
                                    <th>Product Name</th>
                                    <th>Price($)</th>
                                    <th>Description</th>
                                    <th>Category</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.props.productsData.map((productObject) => {

                                        return (
                                            <tr key={productObject.id}>
                                                <td>
                                                    <span className="custom-checkbox">
                                                        <input type="checkbox"
                                                            id={"checkbox_" + productObject.id}
                                                            onChange={(event) => {
                                                                this.handleCheckbox(event);
                                                            }} />
                                                        <label htmlFor={"checkbox_" + productObject.id}></label>
                                                    </span>
                                                </td>
                                                <td>{productObject.title}</td>
                                                <td>{productObject.price}</td>
                                                <td>{productObject.description}</td>
                                                <td>{productObject.category}</td>
                                                <td className='text-center'>
                                                    <Link to={`/updateProduct/${productObject.id}`}
                                                        className="btn btn-warning btn-sm">
                                                        <i className="fa-solid fa-pen"></i>
                                                        &nbsp;
                                                        <span>Edit</span>
                                                    </Link>
                                                    <div className="btn btn-danger btn-sm"
                                                        onClick={() => {
                                                            this.initiateDelete([productObject.id]);
                                                        }} >
                                                        <i className="fa-regular fa-square-minus"></i>
                                                        &nbsp;
                                                        <span>Delete</span>
                                                    </div>
                                                </td>
                                            </tr>
                                        );
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>
        );
    }
}

export default Inventory;