import React from 'react';
import { Link } from 'react-router-dom';
import validator from 'validator';

class AddProduct extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            inputProductName: null,
            inputProductPrice: null,
            inputProductDescription: null,
            inputProductCategory: null,
            inputImageURL: null,

            isFormValid: null,
            inputProductNameIsValid: null,
            inputProductPriceIsValid: null,
            inputProductDescriptionIsValid: null,
            inputProductCategoryIsValid: null,
            inputImageURLIsValid: null,
            showForm: true,
            onSubmitMessage: "Please enter following details-",
        };

        this.previewImageUrl = "https://imgs.search.brave.com/LhBj3yNsORACljjL32pO7PQM07nDnRxNt9nYTc6v7F0/rs:fit:1024:1024:1/g:ce/aHR0cHM6Ly9jZG4x/Lmljb25maW5kZXIu/Y29tL2RhdGEvaWNv/bnMvbW9kZXJuLXVu/aXZlcnNhbC8zMi9p/Y29uLTIxLTEwMjQu/cG5n";
    };


    validateFormInput = (inputElementId) => {

        switch (inputElementId) {

            case "inputProductName":
                if (validator.isEmpty(this.state.inputProductName) === true) {
                    this.setState({
                        isFormValid: false,
                        inputProductNameIsValid: false,

                    });
                } else {
                    this.setState({
                        inputProductNameIsValid: true,
                    });
                }

                break;
            case "inputProductPrice":
                if (validator.isEmpty(this.state.inputProductPrice) === true ||
                    Number(this.state.inputProductPrice) <= 0) {
                    this.setState({
                        isFormValid: false,
                        inputProductPriceIsValid: false,

                    });
                } else {
                    this.setState({
                        inputProductPriceIsValid: true,
                    });
                }

                break;
            case "inputProductDescription":
                if (validator.isEmpty(this.state.inputProductDescription) === true) {
                    this.setState({
                        isFormValid: false,
                        inputProductDescriptionIsValid: false,

                    });
                } else {
                    this.setState({
                        inputProductDescriptionIsValid: true,
                    });
                }

                break;
            case "inputProductCategory":
                if (this.state.inputProductCategory === "Choose...") {
                    this.setState({
                        isFormValid: false,
                        inputProductCategoryIsValid: false,
                    });
                } else {
                    this.setState({
                        inputProductCategoryIsValid: true,
                    });
                }


                break;
            case "inputImageURL":
                if (validator.isEmpty(this.state.inputImageURL) === true ||
                    validator.isURL(this.state.inputImageURL) === false ||
                    this.state.inputImageURL.endsWith('jpg') === false) {
                    this.setState({
                        isFormValid: false,
                        inputImageURLIsValid: false,

                    });
                } else {
                    this.setState({
                        inputImageURLIsValid: true,
                    });
                }

                break;
            default:
                console.error(`${inputElementId}ID does not match state variables!`);
        };


    };

    capitalizeString = (string) => {
        if (typeof string === "string") {
            return string.split(" ")
                .map((word) => {
                    return word[0].toUpperCase() + word.substring(1);
                })
                .join(" ");
        } else {
            return null;
        }
    }

    handleUserInput = (event) => {

        if (event.target.id === "inputProductName") {
            event.target.value = this.capitalizeString(event.target.value);

        }

        this.setState({
            [event.target.id]: event.target.value,

        },
            () => {
                this.validateFormInput(event.target.id);

            }
        );


    };

    displayErrors = () => {
        this.setState((prevState) => {

            return {
                inputProductNameIsValid: (prevState.inputProductNameIsValid === true),
                inputProductPriceIsValid: (prevState.inputProductPriceIsValid === true),
                inputProductDescriptionIsValid: (prevState.inputProductDescriptionIsValid === true),
                inputProductCategoryIsValid: (prevState.inputProductCategoryIsValid === true),
                inputImageURLIsValid: (prevState.inputImageURLIsValid === true),
            };

        });
    }

    createNewProduct = (event) => {
        event.preventDefault();
        this.displayErrors();

        let isFormValid = false
        if (this.state.inputProductNameIsValid &&
            this.state.inputProductPriceIsValid &&
            this.state.inputProductDescriptionIsValid &&
            this.state.inputProductCategoryIsValid &&
            this.state.inputImageURLIsValid
        ) {
            isFormValid = true;

        }

        if (isFormValid === true) {
            this.setState((prevState) => {
                return {
                    isFormValid: true,
                    showForm:false,
                    onSubmitMessage: `Product ${prevState.inputProductName} added to Inventory!`
                };
            },
                () => {
                    let productObject = {
                        title: this.state.inputProductName,
                        price: Number.parseFloat(this.state.inputProductPrice),
                        description: this.state.inputProductDescription,
                        category: this.state.inputProductCategory,
                        image: this.state.inputImageURL,
                        rating: {
                            rate: "no-rating",
                            count: 0
                        }
                    };
                    this.props.addProductHandler(productObject);
                }
            );

        } else {
            this.setState({
                onSubmitMessage: "Invalid form inputs, please fill every detail properly!",
            },
                () => {
                    console.log("Invalid Form");
                }
            );
        }


    };


    render() {
        return (
            <main className="flex-grow-1 col-md-6 col-10 mx-auto my-2 bg-dark text-light p-2 rounded min-vh-100">
                <div className="col-md-12">
                    <h3>{this.state.onSubmitMessage}</h3>
                </div>
                {
                    this.state.showForm === true &&
                    <form className="row g-3 p-3">
                        <div className="col-md-12">
                            <label htmlFor="inputProductName"
                                className="form-label">Product Name</label>
                            <input type="text"
                                className={
                                    this.state.inputProductNameIsValid === false ?
                                        "form-control is-invalid" :
                                        this.state.inputProductNameIsValid === true ?
                                            "form-control is-valid" : "form-control"
                                }
                                id="inputProductName"
                                onChange={(event) => { this.handleUserInput(event) }}
                            />
                            <div className="invalid-feedback">
                                Product Name can not be empty!
                            </div>
                            <div className="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                        <div className="col-md-12">
                            <label htmlFor="inputProductDescription"
                                className="form-label">Product Description</label>
                            <textarea
                                className={
                                    this.state.inputProductDescriptionIsValid === false ?
                                        "form-control is-invalid" :
                                        this.state.inputProductDescriptionIsValid === true ?
                                            "form-control is-valid" : "form-control"
                                }
                                id="inputProductDescription"
                                onChange={(event) => { this.handleUserInput(event) }}
                            />
                            <div className="invalid-feedback">
                                Product Description can not be empty!
                            </div>
                            <div className="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                        <div className="row col-md-6">
                            <div className="col-md-12">
                                <label htmlFor="inputProductPrice"
                                    className="form-label">Product Price in $</label>
                                <input type="number"
                                    className={
                                        this.state.inputProductPriceIsValid === false ?
                                            "form-control is-invalid" :
                                            this.state.inputProductPriceIsValid === true ?
                                                "form-control is-valid" : "form-control"
                                    }
                                    id="inputProductPrice"
                                    onChange={(event) => { this.handleUserInput(event) }}
                                />
                                <div className="invalid-feedback">
                                    Product Price can not be empty! It should be positive number!
                                </div>
                                <div className="valid-feedback">
                                    Looks good!
                                </div>
                            </div>
                            <div className="col-md-12">
                                <label htmlFor="inputImageURL"
                                    className="form-label">Product Image URL</label>
                                <input type="text"
                                    className={
                                        this.state.inputImageURLIsValid === false ?
                                            "form-control is-invalid" :
                                            this.state.inputImageURLIsValid === true ?
                                                "form-control is-valid" : "form-control"
                                    }
                                    id="inputImageURL"
                                    onChange={(event) => { this.handleUserInput(event) }}
                                />
                                <div className="invalid-feedback">
                                    Product image url can not be empty and should be a valid image url(.jpg)!
                                </div>
                                <div className="valid-feedback">
                                    Looks good!
                                </div>
                            </div>
                            <div className="col-md-12">
                                <label htmlFor="inputProductCategory"
                                    className="form-label">Product Category</label>
                                <select id="inputProductCategory"
                                    className={
                                        this.state.inputProductCategoryIsValid === false ?
                                            "form-control is-invalid" :
                                            this.state.inputProductCategoryIsValid === true ?
                                                "form-control is-valid" : "form-control"
                                    }
                                    onChange={(event) => { this.handleUserInput(event) }}
                                >
                                    <option defaultValue>Choose...</option>
                                    <option>men's clothing</option>
                                    <option>jewelery</option>
                                    <option>electronics</option>
                                    <option>women's clothing</option>
                                    <option>other</option>
                                </select>
                                <div className="invalid-feedback">
                                    Please select one of the categories!
                                </div>
                                <div className="valid-feedback">
                                    Looks good!
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="productPreview"
                                className="form-label">Product Preview</label>
                            <div>
                                {
                                    this.state.inputImageURLIsValid === true ?
                                        <img src={this.state.inputImageURL}
                                            alt="Preview of product"
                                            className="img-thumbnail w-100 h-auto" />
                                        :
                                        <img src={this.previewImageUrl}
                                            alt="Preview of product"
                                            className="img-thumbnail w-100 h-auto" />

                                }
                            </div>
                        </div>
                        <div className="col-md-12 mt-4 text-center">
                            <button type="submit"
                                className="btn btn-success"
                                onClick={(event) => { this.createNewProduct(event) }}>
                                Add product
                            </button>
                        </div>

                    </form>
                }
                <Link to={`/`}>
                    <div className="col-md-12 mt-4 text-center">
                        <button
                            className="btn btn-warning">
                            Go back to Inventory
                        </button>
                    </div>
                </Link>
            </main>

        );
    }
}

export default AddProduct;