import React from "react";

class HeaderFakeStore extends React.Component {

    render() {
        return (
            <div className="container-fluid bg-dark text-light p-0 w-100">
                <header className="navbar text-center d-flex align-content-center">
                    <h4 className="m-0"><i className="fa-solid fa-store"></i> Fake Store</h4>
                    <h2 className="m-0">Inventory</h2>
                </header>
            </div >
        );
    }
}

export default HeaderFakeStore;


