import React from 'react';

class ButtonConfirm extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <p>Confirm {this.props.buttonMessage}?</p>
                <div className="btn btn-primary"
                    onClick={() => {
                        this.props.buttonHandler(true);
                    }}>
                    Yes
                </div>
                <div className="btn btn-secondary"
                    onClick={() => {
                        this.props.buttonHandler(false);
                    }}>
                    No
                </div>

            </div>
        );
    }
}

export default ButtonConfirm;