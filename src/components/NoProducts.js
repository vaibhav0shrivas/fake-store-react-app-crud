import React from 'react';


function NoProducts() {
    return (
        <div className="no-products text-center" >
            <h2>Sorry the Product you are looking for does not exist.</h2>
        </div>
    );
}

export default NoProducts;