import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Axios from "axios";

import "./App.css";
import FooterFakeStore from "./components/FooterFakeStore";
import HeaderFakeStore from "./components/HeaderFakeStore";
import WeFailed from "./components/WeFailed";
import Loader from "./components/Loader";
import Inventory from "./components/Inventory";
import NoProducts from "./components/NoProducts";
import AddProduct from "./components/AddProduct";
import UpdateProduct from "./components/UpdateProduct";

class App extends React.Component {

  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
      NOPRODUCTEXIST: "showNoProductExist",
    };

    this.productsURL = "https://fakestoreapi.com/products/";
    this.newId = null;

    this.state = {
      productsData: null,
      status: this.API_STATES.LOADING,

    };

  };

  getProductsFromServer = () => {

    this.setState({
      status: this.API_STATES.LOADING,
    },
      () => {
        Axios.get(this.productsURL)
          .then((response) => {
            if (response.status !== 200) {
              throw Error("Fetch Failed");
            }
            return response.data;

          })
          .then((data) => {


            if (data.length === 0) {
              this.setState({
                status: this.API_STATES.NOPRODUCTEXIST,
              });

            } else {
              this.newId = data[data.length - 1].id + 1;
              this.setState({
                status: this.API_STATES.LOADED,
                productsData: data,
              });

            }
          })
          .catch((error) => {
            this.setState({
              status: this.API_STATES.ERROR,

            },
              () => {
                console.error(error);
              }
            );
          });


      }
    );

  };


  //Create
  addProductToProductList = (productObject) => {

    let copyOfNewProduct = {
      ...productObject,
      id: this.newId,
    };
    this.newId++;
    let copyOfProductData = [...this.state.productsData, copyOfNewProduct];

    this.setState({
      productsData: [...copyOfProductData],
    });



  };



  //Update
  updateProductInProductList = (productID, newProductObject) => {

    this.setState((prevState) => {
      let oldProductObject = prevState.productsData.find((product) => {
        return String(product.id) === productID;

      });

      let updatedProductObject = Object.assign(
        oldProductObject,
        newProductObject
      );


      let updatedProductsData = prevState.productsData.map((productObject) => {
        if (updatedProductObject.id === productObject.id) {
          return updatedProductObject;
        }
        else {
          return productObject;
        }
      });

      return {
        productsData: [...updatedProductsData],
      }
    });



  };

  //Delete
  deleteProductFromProductList = (productIDArray) => {

    let copyOfProductData = this.state.productsData;
    let updatedProductsData = copyOfProductData.filter((productObject) => {
      if (productIDArray.includes(productObject.id)) {
        return false;
      } else {
        return true;
      }

    })

    this.setState({
      productsData: [...updatedProductsData],
    });



  };



  componentDidMount() {
    this.getProductsFromServer();
  };

  render() {
    return (
      <Router>
        <div className="App d-flex flex-column align-items-center">
          <HeaderFakeStore />

          {
            this.state.status === this.API_STATES.LOADING &&
            <main className="flex-grow-1 min-vh-100">
              <Loader />
            </main>

          }

          {
            this.state.status === this.API_STATES.ERROR &&
            <main className="flex-grow-1 min-vh-100">
              <WeFailed />
            </main>
          }

          {
            this.state.status === this.API_STATES.NOPRODUCTEXIST &&
            <main className="flex-grow-1 min-vh-100">
              <NoProducts />
            </main>
          }

          {
            this.state.status === this.API_STATES.LOADED &&
            <Switch>
              <Route
                exact path="/"
                render={(props) => {
                  return (
                    <Inventory {...props}
                      productsData={this.state.productsData}
                      deleteProductHandler={this.deleteProductFromProductList}
                    />
                  )
                }}
              />

              <Route
                exact path="/addProduct"
                render={(props) => {
                  return (
                    <AddProduct {...props}
                      addProductHandler={this.addProductToProductList}
                    />
                  )
                }}
              />

              <Route
                exact path="/updateProduct/:id"
                render={(props) => {

                  let productToBeEdited = this.state.productsData.find((product) => {
                    return String(product.id) === props.match.params.id;

                  });

                  return (
                    <UpdateProduct {...props}
                      productToBeEdited={productToBeEdited}
                      updateProductHandler={this.updateProductInProductList}
                      deleteProductHandler={this.deleteProductFromProductList}
                    />
                  )
                }}
              />

              <Route
                exact path="/updateProduct"
                render={() => {
                  return (
                    <main className="flex-grow-1 min-vh-100">
                      <NoProducts />
                    </main>
                  )
                }}
              />

              <Route
                path="/*"
                render={() => {
                  return (
                    <main className="flex-grow-1 min-vh-100">
                      <WeFailed />
                    </main>
                  )
                }}
              />

            </Switch>
          }

          <FooterFakeStore />
        </div>
      </Router>
    );

  };

};


export default App;

